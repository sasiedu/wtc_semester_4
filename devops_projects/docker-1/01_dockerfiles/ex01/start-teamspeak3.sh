#!/bin/bash

#cd /data

TARFILE=teamspeak3-server_linux_amd64-3.0.13.6.tar.bz2

echo "Downloading ${TARFILE} ..."
wget ENV TEAMSPEAK_URL http://dl.4players.de/ts/releases/3.0.13.6/teamspeak3-server_linux_amd64-3.0.13.6.tar.bz2
tar -vjxf ${TARFILE} --strip-components=1
echo $TS_VERSION >version

export LD_LIBRARY_PATH=/data

TS3ARGS=""
if [ -e /data/ts3server.ini ]; then
	TS3ARGS="inifile=/data/ts3server.ini"
else
	TS3ARGS="createinifile=1"
fi

exec sh ts3server_minimal_runscript.sh

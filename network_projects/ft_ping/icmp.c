/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   icmp.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 09:50:56 by sasiedu           #+#    #+#             */
/*   Updated: 2017/10/31 09:51:02 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ping.h>

ssize_t		icmp_reply(t_echoreply *reply, t_args *args)
{
	int			seq;
	short int	len;

	if (reply->icmp.id != getpid())
		return (0);
	seq = (reply->icmp.seq >> 8) | ((reply->icmp.seq << 8) & 0xFF);
	len = (reply->ip_header.total_len >> 8) | \
			((reply->ip_header.total_len << 8) & 0xFF);
	echo_reply(args, reply, len, seq);
	if (seq == 255)
		g_stats.seq += 256;
	return (1);
}

ssize_t		icmp_ttl(t_echoreply *reply, t_args *args)
{
	t_echoreply		*tmp;
	int				seq;

	(void)args;
	tmp = (t_echoreply *)reply->data;
	if (tmp->icmp.id != getpid())
		return (0);
	seq = (tmp->icmp.seq >> 8) | ((tmp->icmp.seq << 8) & 0xFF);
	printf("From %s (%s) icmp_seq=%ld Time to live exceeded\n",
			inet_ntoa(reply->ip_header.src), inet_ntoa(reply->ip_header.src),
			seq + g_stats.seq);
	if (seq == 255)
		g_stats.seq += 256;
	return (1);
}

ssize_t		icmp_dest(t_echoreply *reply, t_args *args)
{
	t_echoreply		*tmp;
	int				seq;
	char			*error;

	(void)args;
	tmp = (t_echoreply *)reply->data;
	if (tmp->icmp.id != getpid())
		return (0);
	error = "Network unreachable";
	if (reply->icmp.code == HOST_UNREACHABLE)
		error = "Host unreachable";
	else if (reply->icmp.code == PROTOCOL_UNREACHABLE)
		error = "Protocol unreachable";
	else if (reply->icmp.code == PORT_UNREACHABLE)
		error = "Port unreachable";
	seq = (tmp->icmp.seq >> 8) | ((tmp->icmp.seq << 8) & 0xFF);
	printf("Fromm %s (%s) icmp_seq=%ld %s\n",
			inet_ntoa(reply->ip_header.src), inet_ntoa(reply->ip_header.src),
			seq + g_stats.seq, error);
	if (seq == 255)
		g_stats.seq += 256;
	return (1);
}

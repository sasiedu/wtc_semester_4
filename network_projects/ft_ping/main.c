/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/19 22:14:05 by sasiedu           #+#    #+#             */
/*   Updated: 2017/10/31 09:51:40 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ping.h>

t_stat	g_stats = {0.0, 0.0, 0.0, 0, 0, NULL, 0, 0};

void	setup(struct addrinfo *dest_addr, t_args *args)
{
	int				raw_socket;
	struct timeval	t;
	struct timezone	tz;

	if ((raw_socket = create_socket(args)) == -1)
		return ;
	printf("PING %s (%s): %zu(%zu) bytes of data.\n", \
		dest_addr->ai_canonname, \
		inet_ntoa(((struct sockaddr_in *)dest_addr->ai_addr)->sin_addr),\
		args->buf_len, args->buf_len + 28);
	raw_socket = set_socket_opts(raw_socket, args);
	g_stats.name = dest_addr->ai_canonname;
	signal(SIGINT, signal_handler);
	gettimeofday(&t, &tz);
	g_stats.start_time = ((float)t.tv_usec / 1000.0 + (1000.0 * t.tv_sec));
	while (1)
	{
		ping(raw_socket, args, dest_addr, getuid());
	}
}

int		main(int ac, char **av)
{
	struct addrinfo		*dest_addr;
	t_args				args;

	if (ac == 1)
		return (print_usage());
	av[ac] = NULL;
	args = load_opts(MAKE_PING(), ++av);
	if (args.help)
		return (print_usage());
	if ((dest_addr = get_address_info(&args)) == NULL)
	{
		printf("ft_ping : cannot resolve %s: Unkown host\n", args.addr);
		return (1);
	}
	setup(dest_addr, &args);
	return (0);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ping.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/21 12:48:38 by sasiedu           #+#    #+#             */
/*   Updated: 2017/10/26 10:33:41 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ping.h>

t_echoreq	*create_echo_request(t_echoreq *req, t_args *args, uid_t uid)
{
	static uint16_t	seq = 0;
	struct timezone	tz;
	struct timeval	t;

	(void)uid;
	req->icmp.type = 8;
	req->icmp.code = 0;
	req->icmp.checksum = 0;
	req->icmp.id = getpid();
	req->icmp.seq = (seq >> 8) | (seq << 8);
	memset(req->data, 'E', args->buf_len - 8);
	gettimeofday(&t, &tz);
	req->time = ((float)t.tv_usec / 1000.0) + (1000.0 * t.tv_sec);
	req->icmp.checksum = checksum(req, \
		sizeof(t_icmp_header) + args->buf_len);
	seq += 1;
	return (req);
}

int		ping(int sock, t_args *args, struct addrinfo *dest_addr, uid_t uid)
{
	t_echoreq		req;
	t_echoreply		reply;
	int				ret;

	memset((void *)&req, 0, sizeof(t_echoreq));
	memset((void *)&reply, 0, sizeof(t_echoreply));
	create_echo_request(&req, args, uid);
	if ((ret = send_packet(sock, args, dest_addr, &req)) < 0)
		return (ret);
	if (recv_packet(sock, &reply, args) < 1)
		printf("Request time out\n");
	usleep(args->wait * 1000);
	return (0);
}

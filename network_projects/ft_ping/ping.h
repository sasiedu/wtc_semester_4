/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ping.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/19 22:20:33 by sasiedu           #+#    #+#             */
/*   Updated: 2017/10/31 10:07:09 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PING_H
# define PING_H

# include <structs.h>
# include <errno.h>

/*
*** Args functions
*/
struct addrinfo		*get_address_info(t_args *args);
t_args				load_opts(t_args args, char **av);

/*
*** Util functions
*/
int					print_usage(void);
uint16_t			checksum(void *b, int len);
uint16_t			extract_uint16(uint16_t val);
void				signal_handler(int sig);
void				set_time_stats(float time);
int					is_number(char *str);
long int			get_number_opt(char *opt, char *optval);

/*
*** Socket functions
*/
int					create_socket(t_args *args);
int					send_packet(int sock, t_args *args, \
						struct addrinfo *dest_addr, t_echoreq *req);
ssize_t				recv_packet(int sock, t_echoreply *reply, t_args *args);
int					set_socket_opts(int sock, t_args *args);

/*
*** Ping functions
*/
t_echoreq			*create_echo_request(t_echoreq *req, t_args *args, \
						uid_t uid);
int					ping(int sock, t_args *args, struct addrinfo \
						*dest_addr, uid_t uid);

/*
*** Types functions
*/
int					echo_reply(t_args *args, t_echoreply *reply, \
						short int len, int seq);

/*
*** Icmp functions
*/
ssize_t				icmp_reply(t_echoreply *reply, t_args *args);
ssize_t				icmp_ttl(t_echoreply *reply, t_args *args);
ssize_t				icmp_dest(t_echoreply *reply, t_args *args);

#endif

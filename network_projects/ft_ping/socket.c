/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   socket.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/21 10:33:39 by sasiedu           #+#    #+#             */
/*   Updated: 2017/10/31 09:58:02 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ping.h>

int		set_socket_opts(int sock, t_args *args)
{
	setsockopt(sock, IPPROTO_IP, IP_TTL, &args->ttl, sizeof(args->ttl));
	return (sock);
}

int		create_socket(t_args *args)
{
	int				raw_socket;
	struct timeval	timeout;

	if ((raw_socket = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP)) < 0)
	{
		perror("Error: socket");
		return (raw_socket);
	}
	timeout.tv_sec = args->replywait / 1000;
	timeout.tv_usec = (args->replywait % 1000) * 1000;
	if (setsockopt(raw_socket, SOL_SOCKET, SO_RCVTIMEO, \
				(char *)&timeout, sizeof(timeout)) < 0)
	{
		perror("Error: setsockopt");
		return (-1);
	}
	return (raw_socket);
}

int		send_packet(int sock, t_args *args, struct addrinfo *dest_addr, \
			t_echoreq *req)
{
	ssize_t			ret;
	size_t			data_size;

	data_size = sizeof(t_icmp_header) + args->buf_len;
	ret = sendto(sock, (BYTE *)req, data_size, 0, \
			dest_addr->ai_addr, sizeof(struct sockaddr));
	g_stats.sent++;
	return (ret);
}

ssize_t	check_recv(t_echoreply *reply, t_args *args)
{
	if (reply->icmp.type == ECHO_REPLY)
		return (icmp_reply(reply, args));
	if (reply->icmp.type == TIME_EXCEEDED && reply->icmp.code == TTL_ZERO)
		return (icmp_ttl(reply, args));
	if (reply->icmp.type == DESTINATION_ERROR)
		return (icmp_dest(reply, args));
	return (0);
}

ssize_t	recv_packet(int sock, t_echoreply *reply, t_args *args)
{
	ssize_t			ret;
	struct iovec	iov;
	struct msghdr	msg;
	char			buffer[1024];

	memset((void *)&msg, 0, sizeof(struct msghdr));
	memset((void *)&iov, 0, sizeof(struct iovec));
	memset(buffer, 0, 1024);
	iov.iov_base = reply;
	iov.iov_len = sizeof(t_echoreply);
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;
	msg.msg_flags = 0;
	msg.msg_control = buffer;
	msg.msg_controllen = sizeof(buffer);
	ret = recvmsg(sock, &msg, MSG_WAITALL);
	if (ret < 1)
		return (ret);
	if ((ret = check_recv(reply, args)) == 1)
		return (1);
	return (recv_packet(sock, reply, args));
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   structs.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/19 22:20:53 by sasiedu           #+#    #+#             */
/*   Updated: 2017/10/31 10:06:41 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STRUCTS_H
# define STRUCTS_H
# define TRUE 1
# define FALSE 0
# define BYTE unsigned char
# define ICMP_PROTOCOL 0x01
# define ECHO_REQUEST 8

# include <string.h>
# include <stdint.h>
# include <unistd.h>
# include <stdio.h>
# include <stdlib.h>
# include <sys/types.h>
# include <sys/socket.h>
# include <netdb.h>
# include <arpa/inet.h>
# include <sys/time.h>
# include <signal.h>
# include <math.h>

# define ECHO_REQUEST 8
# define ECHO_REPLY 0
# define DESTINATION_ERROR 3
# define NETWORK_UNREACHABLE 0
# define HOST_UNREACHABLE 1
# define PROTOCOL_UNREACHABLE 2
# define PORT_UNREACHABLE 3
# define TIME_EXCEEDED 11
# define TTL_ZERO 0
# define MAX_PACKET_SIZE 1024

typedef	struct		s_stat
{
	float			min;
	float			max;
	double			avg;
	ssize_t			recv;
	ssize_t			sent;
	char			*name;
	double			start_time;
	uint64_t		seq;
}					t_stat;

extern t_stat		g_stats;

typedef struct		s_args
{
	int				verbose;
	int				help;
	size_t			buf_len;
	long int		ttl;
	long int		timeout;
	int				tos;
	int				only_one;
	long int		wait;
	long int		replywait;
	char			*addr;
}					t_args;

typedef struct		s_icmp_header
{
	uint8_t			type;
	uint8_t			code;
	uint16_t		checksum;
	uint16_t		id;
	uint16_t		seq;
}					t_icmp_header;

typedef struct		s_echoreq
{
	t_icmp_header	icmp;
	double			time;
	BYTE			data[MAX_PACKET_SIZE];
}					t_echoreq;

/*
*** vihl -> version, header length
*** tos -> type of service
*** ttl -> time to live
*/

typedef struct		s_ip_header
{
	uint8_t			vihl;
	uint8_t			tos;
	short int		total_len;
	short int		id;
	short int		flagoff;
	uint8_t			ttl;
	uint8_t			protocol;
	short int		checksum;
	struct in_addr	src;
	struct in_addr	dest;
}					t_ip_header;

typedef struct		s_echoreply
{
	t_ip_header		ip_header;
	t_icmp_header	icmp;
	BYTE			data[MAX_PACKET_SIZE];
}					t_echoreply;

# define ECHO_REQ_SIZE sizeof(t_echoreq)
# define ECHO_REPLY_SIZE sizeof(t_echoreply)
# define MAKE_PING() ((t_args) {0, 0, 56, 64, (-1), 0, 0, 1000, 1000, NULL})
# define MALLOC(x) ((void *)malloc(sizeof(char) * x))
# define BYTE_COPY(x,y,len) (memcpy((BYTE *)x, (BYTE *)y, len))
# define MAKE_ICMP_HDR(x, y, z, w) ((t_icmp_header){x, y, 0, z, w})
# define ECHO_REQ(x,y,z) ((t_echoreq){MAKE_ICMP_HDR(8,0,x,y), 0, MALLOC(z)})

#endif

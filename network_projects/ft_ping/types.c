/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   types.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/22 13:07:57 by sasiedu           #+#    #+#             */
/*   Updated: 2017/10/31 09:57:02 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ping.h>

int		echo_reply(t_args *args, t_echoreply *reply, short int len, int seq)
{
	struct timezone tz;
	struct timeval	t;
	double			time;
	double			snd_time;

	g_stats.recv++;
	gettimeofday(&t, &tz);
	memcpy(&snd_time, reply->data, sizeof(double));
	time = ((float)t.tv_usec / 1000.0 + (1000.0 * t.tv_sec)) - snd_time;
	set_time_stats(time);
	printf("%d bytes from %s: icmp_seq=%ld ttl=%d time=%.3f ms%s\n",\
		len - 20, inet_ntoa(reply->ip_header.src), g_stats.seq + seq, \
		reply->ip_header.ttl, time, ((len - 28) != (long int)args->buf_len) \
		? " (truncated)" : "");
	if (args->only_one)
		signal_handler(SIGINT);
	return (TRUE);
}

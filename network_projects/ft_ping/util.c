/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   util.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/19 23:15:40 by sasiedu           #+#    #+#             */
/*   Updated: 2017/10/31 10:04:45 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ping.h>

int			print_usage(void)
{
	printf("ft_ping: option requires an argument\n");
	printf("usage: ft_ping [-vh] host\n");
	return (1);
}

uint16_t	checksum(void *b, int len)
{
	unsigned short	*buf;
	unsigned int	sum;
	unsigned short	result;

	buf = (unsigned short *)b;
	sum = 0;
	while (len > 1)
	{
		sum += *buf++;
		len -= 2;
	}
	if (len == 1)
		sum += *(unsigned char*)buf;
	sum = (sum >> 16) + (sum & 0xFFFF);
	sum += (sum >> 16);
	result = ~sum;
	return (result);
}

uint16_t	extract_uint16(uint16_t val)
{
	BYTE	*data;

	data = (BYTE *)&val;
	return ((data[1] << 0) | (data[0] << 8));
}

void		signal_handler(int sig)
{
	int				lost;
	float			avg;
	struct timeval	t;
	struct timezone	tz;

	(void)sig;
	gettimeofday(&t, &tz);
	lost = g_stats.sent - g_stats.recv;
	avg = g_stats.avg / (double)g_stats.recv;
	printf("\n--- %s ping statistics ---\n", g_stats.name);
	printf("%ld packets transmitted, %ld received, ",
			g_stats.sent, g_stats.recv);
	printf("%.1f%% packet loss, time %ldms\n",
			((float)lost / (float)g_stats.sent) * 100.0, \
			(long)((float)t.tv_usec / 1000.0 + (1000.0 * t.tv_sec)) - \
			(long)g_stats.start_time);
	if (g_stats.min > 0.0f)
	{
		printf("rtt min/avg/max/mdev = %.3f/%.3f/%.3f/%.3f ms\n",
			g_stats.min, avg, g_stats.max,
			sqrt(((avg * avg) / (float)g_stats.recv) - \
			(avg / (float)g_stats.recv)));
	}
	exit(0);
}

void		set_time_stats(float time)
{
	if (g_stats.min == 0.0f || time < g_stats.min)
		g_stats.min = time;
	if (g_stats.max == 0.0f || time > g_stats.max)
		g_stats.max = time;
	g_stats.avg += time;
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   util1.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 10:02:06 by sasiedu           #+#    #+#             */
/*   Updated: 2017/10/31 10:04:29 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ping.h>
#include <ctype.h>

void		reverse_bytes(void *bits, size_t n)
{
	size_t	half;
	size_t	i;
	BYTE	c;
	BYTE	*data;

	half = n / 2;
	i = 0;
	data = (BYTE *)bits;
	while (i < half)
	{
		c = data[i];
		data[i] = data[n - i - 1];
		data[n - i - 1] = c;
		i++;
	}
}

int			is_number(char *str)
{
	if (str == NULL || *str == '\0')
		return (FALSE);
	while (*str != '\0')
	{
		if ((*str > 47 && *str < 58) || *str == 46)
		{
			str++;
			continue ;
		}
		return (FALSE);
	}
	return (TRUE);
}

long int	get_number_opt(char *opt, char *optval)
{
	char		*tmp;
	long int	val;

	val = 0;
	(void)opt;
	if (!is_number(optval))
		return (-1);
	tmp = strchr(optval, '.');
	val = atoi(optval) * 1000;
	if (tmp != NULL)
		val += ((float)atoi(tmp + 1) /\
				pow(10, strlen(tmp + 1))) * 1000;
	return (val);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   args.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/29 08:28:35 by sasiedu           #+#    #+#             */
/*   Updated: 2018/01/29 08:53:53 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <traceroute.h>

struct addrinfo	*find_good_address(struct addrinfo *info)
{
	struct sockaddr_in	*h;

	if (info == NULL)
		return (NULL);
	h = (struct sockaddr_in *)info->ai_addr;
	if (inet_ntoa(h->sin_addr) != NULL)
		return (info);
	return (find_good_address(info->ai_next));
}

struct addrinfo	*get_address_info(t_args *args)
{
	struct addrinfo		hints;
	struct addrinfo		*info;
	struct addrinfo		*tmp;
	int					ret;

	(void)args;
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_flags = AI_CANONNAME;
	if ((ret = getaddrinfo(args->addr, NULL, &hints, &info)) != 0)
	{
		printf("ft_traceroute: Unknown host %s\n",
				args->addr);
		exit(EXIT_FAILURE);
	}
	tmp = find_good_address(info);
	return (tmp);
}

t_args			set_value(t_args args, char *opt, long int val)
{
	if (!strcmp(opt, "-h"))
		args.help = val;
	return (args);
}

t_args			load_opts(t_args args, char **av)
{
	char		*opt;
	long int	optval;

	opt = *av;
	if (opt == NULL)
		return (args);
	if (!strcmp("-v", opt) || !strcmp("-h", opt) || !strcmp("-o", opt))
		return (load_opts(set_value(args, opt, TRUE), ++av));
	if (!strcmp("-t", opt) || !strcmp("-w", opt) || !strcmp("-W", opt) \
			|| !strcmp("-Q", opt) || !strcmp("-i", opt) || \
			!strcmp("-s", opt))
	{
		if ((optval = get_number_opt(opt, *(av + 1))) == -1)
		{
			printf("invalid option value %s : %s\n", opt, *(av + 1));
			exit(EXIT_FAILURE);
		}
		return (load_opts(set_value(args, opt, optval), (av + 2)));
	}
	if (**av != '\0' && **av != '-')
		args.addr = *av;
	return (load_opts(args, ++av));
}

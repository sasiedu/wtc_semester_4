/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/29 08:28:41 by sasiedu           #+#    #+#             */
/*   Updated: 2018/01/29 09:57:37 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <traceroute.h>

int		main(int ac, char **av)
{
	struct addrinfo		*dest_addr;
	t_args				args;

	if (ac == 1)
		return (print_usage());
	av[ac] = NULL;
	args = load_opts(MAKE_PACKET(), ++av);
	if (args.help)
		return (print_usage());
	if ((dest_addr = get_address_info(&args)) == NULL)
	{
		printf("ft_traceroute : Unkown host %s\n", args.addr);
		return (1);
	}
	//puts(dest_addr->ai_canonname);
	//puts(inet_ntoa(((struct sockaddr_in *)dest_addr->ai_addr)->sin_addr));
	printf("ft_traceroute to %s (%s), %zu hops max, %zu byte packets\n", \
			dest_addr->ai_canonname,
			inet_ntoa(((struct sockaddr_in *)dest_addr->ai_addr)->sin_addr),
			args.ttl, args.buf_len);
	return (0);
}

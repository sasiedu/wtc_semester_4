/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   traceroute.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/29 08:28:25 by sasiedu           #+#    #+#             */
/*   Updated: 2018/01/29 08:37:43 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TRACEROUTE_H
# define TRACEROUTE_H

# include <structs.h>
# include <errno.h>

/*
*** Args functions
*/
struct addrinfo		*get_address_info(t_args *args);
t_args				load_opts(t_args args, char **av);

/*
*** Util functions
*/
int					print_usage(void);
uint16_t			checksum(void *b, int len);
uint16_t			extract_uint16(uint16_t val);
void				signal_handler(int sig);
void				set_time_stats(float time);
int					is_number(char *str);
long int			get_number_opt(char *opt, char *optval);

#endif

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   util.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/29 08:31:15 by sasiedu           #+#    #+#             */
/*   Updated: 2018/01/29 08:45:51 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <traceroute.h>

int			print_usage(void)
{
	printf("Usage: ft_traceroute: [-adDeFInrSvx] [-A as_server] ");
	printf("[-f first_ttl] [-g gateway] [-i iface]\n");
	printf("-M first_ttl] [-m max_ttl] [-p port] ");
	printf("[-P proto] [-q nqueries] [-s src_addr]\n");
	printf("[-t tos] [-w waittime] [-z pausemsecs] host [packetlen]\n");
	return (1);
}

uint16_t	checksum(void *b, int len)
{
	unsigned short	*buf;
	unsigned int	sum;
	unsigned short	result;

	buf = (unsigned short *)b;
	sum = 0;
	while (len > 1)
	{
		sum += *buf++;
		len -= 2;
	}
	if (len == 1)
		sum += *(unsigned char*)buf;
	sum = (sum >> 16) + (sum & 0xFFFF);
	sum += (sum >> 16);
	result = ~sum;
	return (result);
}

uint16_t	extract_uint16(uint16_t val)
{
	BYTE	*data;

	data = (BYTE *)&val;
	return ((data[1] << 0) | (data[0] << 8));
}

void		signal_handler(int sig)
{
	(void)sig;
	exit(0);
}

void		set_time_stats(float time)
{
	(void)time;
}

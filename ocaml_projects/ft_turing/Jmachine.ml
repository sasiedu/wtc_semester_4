(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   Jmachine.ml                                        :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2017/10/17 18:35:44 by sasiedu           #+#    #+#             *)
(*   Updated: 2017/10/19 16:15:47 by sasiedu          ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

type json_machine = {
    name : string;
    work : char list;
    inputs : char list;
    blank : char;
    states : string list;
    initial_state : string;
    final_states : string list;
    transitions : Transition.transition list;
}

let get_transition machine state head =
    Transition.get_transition state head machine.transitions

let get_value_from_json value json = 
    match (json |> Yojson.Basic.Util.member value) with
    | `Null -> Printf.printf "%s not found in JSON file\n" value; exit 0
    | obj -> obj

let convert_string_to_char str = match (String.length str) with
    | 3 -> 
        begin
        if (str.[0] = '"') && (str.[2] = '"') then 
            str.[1]
        else 
            (fun () -> str |> Printf.printf "%s is invalid\n"; exit 0)()
        end
    | _ -> str |> Printf.printf "%s has length > 1\n"; exit 0

let get_inputs json_machine = json_machine.inputs
let get_blank json_machine = json_machine.blank

let print_json_machine machine =
    Printf.printf "************************************************\n";
    Printf.printf "Name : %s\n" machine.name;
    Printf.printf "Alphabet : [";
    machine.work |> List.iter (fun x -> x |> Printf.printf " %c,");
    Printf.printf "]\nInputs : [";
    machine.inputs |> List.iter (fun x -> x |> Printf.printf " %c,");
    Printf.printf "]\nBlank : %c\n" machine.blank;
    Printf.printf "States : [";
    machine.states |> List.iter (fun x -> x |> Printf.printf " %s,");
    Printf.printf "]\nInitial : %s\n" machine.initial_state;
    Printf.printf "Finals : [";
    machine.final_states |> List.iter (fun x -> x |> Printf.printf " %s,");
    Printf.printf "]\nTransitions : [\n";
    machine.transitions |> Transition.print_transition_list;
    Printf.printf "]\n";
    Printf.printf "************************************************\n"

let create_json_machine file_name =
    try (fun () ->
      let json = Yojson.Basic.from_file file_name in
      let alpha = json |> get_value_from_json "alphabet" |>
                Yojson.Basic.Util.convert_each (fun x ->
                x |> Yojson.Basic.to_string |> convert_string_to_char); in
      let empty = json |> get_value_from_json "blank" |>
                  Yojson.Basic.to_string |> convert_string_to_char; in
      {
        name = json |> get_value_from_json "name" |> 
               Yojson.Basic.Util.to_string;
        work = alpha;
        blank = empty;
        inputs = alpha |> List.filter (fun x ->
            if (x = empty) then false else true);
        states = json |> get_value_from_json "states" |>
                 Yojson.Basic.Util.convert_each (fun x ->
                    x |> Yojson.Basic.to_string);
        initial_state = json |> get_value_from_json "initial" |>
                        Yojson.Basic.Util.to_string;
        final_states = json |> get_value_from_json "finals" |>
                       Yojson.Basic.Util.convert_each (fun x ->
                        x |> Yojson.Basic.to_string);
        transitions = json |> Transition.load_transitions_to_list;
      }
    )() with
      _ -> file_name |> Printf.printf "%s: No such file\n"; exit 0

let test1 () =
  let json = Yojson.Basic.from_file "unary_sub.json" in
  let name = json |> get_value_from_json "name" |> 
             Yojson.Basic.Util.to_string in
  let alpha = json |> get_value_from_json "alphabet" |>
              Yojson.Basic.Util.convert_each (fun x ->
                  x |> Yojson.Basic.to_string |>
                  convert_string_to_char) in
  Printf.printf "Name : %s\n" name;
  List.iter (fun x -> x |> Printf.printf "%c ") alpha;
  Printf.printf "\n"

let test2 () =
  let json_machine = create_json_machine "unary_sub.json" in
  print_json_machine json_machine

(*let () = test2 ()*)

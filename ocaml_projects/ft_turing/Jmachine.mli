(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   Jmachine.mli                                       :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2017/10/17 18:35:10 by sasiedu           #+#    #+#             *)
(*   Updated: 2017/10/19 16:35:35 by sasiedu          ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

type json_machine = {
    name : string;
    work : char list;
    inputs : char list;
    blank : char;
    states : string list;
    initial_state : string;
    final_states : string list;
    transitions : Transition.transition list;
}

val create_json_machine : string -> json_machine

val print_json_machine : json_machine -> unit

val get_transition : json_machine -> string -> char -> Transition.transition

val get_inputs : json_machine -> char list

val get_blank : json_machine -> char


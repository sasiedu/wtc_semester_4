(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   Tape.ml                                            :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2017/10/18 08:39:32 by sasiedu           #+#    #+#             *)
(*   Updated: 2017/10/19 16:41:58 by sasiedu          ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

module type SINGLE_TAPE =
sig
    type t

    val length : t array -> int
    val create : int -> t -> t array
    val move_left : (int * t array) -> t -> (int * t array)
    val move_right : (int * t array) -> t -> (int * t array)
    val read : (int * t array) -> t
    val write : t -> (int * t array) -> unit
    val print : (int * t array) -> unit
end

module type TAPE =
sig
    type t_in

    val length : t_in array -> int
    val create : int -> t_in -> t_in array
    val move_left : (int * t_in array) -> t_in -> (int * t_in array)
    val move_right : (int * t_in array) -> t_in -> (int * t_in array)
    val read : (int * t_in array) -> t_in
    val write : t_in -> (int * t_in array) -> unit
    val print : (int * t_in array) -> unit
end

module type MAKETAPE =
    functor (Tape : SINGLE_TAPE) ->
        TAPE with type t_in = Tape.t

module MakeTape : MAKETAPE =
    functor (Tape : SINGLE_TAPE) ->
        struct
            type t_in = Tape.t
            let length tape = Tape.length tape
            let create size empty = Tape.create size empty
            let move_left machine empty = Tape.move_left machine empty
            let move_right machine empty = Tape.move_right machine empty
            let read machine = Tape.read machine
            let write c machine = Tape.write c machine
            let print machine = Tape.print machine
        end

module CharTape : (TAPE with type t_in = char) = MakeTape (
struct
    type t = char

    let length tape =
        tape |> Array.length

    let create size empty =
        empty |> Array.make size

    let move_left machine empty = match (fst machine) with
        | 0 ->  let len = machine |> snd |> length in
                let head = len |> (fun x -> x - 1) in
                let tape = machine |> snd |> 
                        Array.append (create len empty) in
                (head, tape)
        | _ -> let head = machine |> fst |> (fun x -> x - 1) in
                (head, machine |> snd)

    let move_right machine empty = match (fst machine) with
        | y when (y + 1) >= (machine |> snd |> length) ->
                let len = machine |> snd |> length in
                let tape = create len empty |> Array.append 
                    (machine |> snd) in
                (machine |> fst |> (fun x -> x + 1), tape)
         | _ -> (machine |> fst |> (fun x -> x + 1), machine |> snd)

    let read machine =
        machine |> fst |> Array.get (machine |> snd)

    let write c machine =
        let index = machine |> fst in
        Array.set (machine |> snd) index c

    let print machine =
        let head = machine |> fst in
        Printf.printf "[";
        Array.iteri (fun index x -> match index with
            | y when y = head -> 
                    print_string "\027[31m";
                    print_char x;
                    print_string "\027[0m"
            | _ -> Printf.printf "%c" x) (machine |> snd);
        Printf.printf "]"
end)

let test1 () =
    let machine1 = ref (0, (CharTape.create 15 '.')) in
    let machine2 = ref (14, (CharTape.create 15 '.')) in
    CharTape.print !machine1;
    CharTape.print !machine2;
    machine1 := CharTape.move_left !machine1 '.';
    CharTape.print !machine1;
    machine2 := CharTape.move_right !machine2 '.';
    CharTape.print !machine2;
    CharTape.write '1' !machine1;
    machine1 := CharTape.move_left !machine1 '.';
    CharTape.write '1' !machine1;
    machine1 := CharTape.move_left !machine1 '.';
    CharTape.write '1' !machine1;
    CharTape.print !machine1

(*let () = test1 ()*)


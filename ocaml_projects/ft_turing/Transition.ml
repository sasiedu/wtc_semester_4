(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   Transition.ml                                      :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2017/10/17 13:35:32 by sasiedu           #+#    #+#             *)
(*   Updated: 2017/10/17 19:47:38 by sasiedu          ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

type actions = LEFT | RIGHT

type transition = {
    current_state : string;
    head_value : char;
    next_state : string;
    write_value : char;
    action : actions;
}

let print_transition obj =
    Printf.printf "\t(%s, %c) -> (%s, %c, %s)\n"
      obj.current_state
      obj.head_value
      obj.next_state
      obj.write_value
      ((fun x -> if x = LEFT then "LEFT" else "RIGHT") obj.action)

let get_value_from_json value json = match (json |> Yojson.Basic.Util.member value) with
    | `Null -> print_endline "Error: Invalid transition in JSON file"; exit 0
    | obj -> (obj |> Yojson.Basic.Util.to_string)

let create_transition state json =
  try
    {
      current_state = state;
      head_value = (fun () ->
            let str = json |> get_value_from_json "read" in
            match (String.length str) with
            | 1 -> str.[0]
            | _ -> failwith "error")();
      next_state = get_value_from_json "to_state" json;
      write_value = (fun () ->
            let str = json |> get_value_from_json "write" in
            match (String.length str) with
            | 1 -> str.[0]
            | _ -> failwith "error")();
      action = (fun x -> match x with
                | "LEFT" -> LEFT
                | "RIGHT" -> RIGHT
                | _ -> print_endline "Invalid transition in JSON file"; exit 0
            )(get_value_from_json "action" json)
    }
   with
     _ -> print_endline "Invalid transition in JSON file"; exit 0

let get_transitions_from_state json =
    let (state, trans) = json |> Yojson.Basic.Util.to_assoc |> List.hd in
    let rec loop t_list = match t_list with
        | [] -> []
        | head::next -> (create_transition state head)::(loop next) in
    trans |> Yojson.Basic.Util.to_list |> loop

let get_transitions_from_state2 (state, trans) =
    let rec loop t_list = match t_list with
        | [] -> []
        | head::next -> (create_transition state head)::(loop next) in
    trans |> Yojson.Basic.Util.to_list |> loop

let load_transitions_to_list json = 
    match (json |> Yojson.Basic.Util.member "transitions") with
    | `Null -> print_endline "Error : No transitions in JSON file"; exit 0
    | `Assoc  a ->
        begin
            let rec loop a_list = match a_list with
                | [] -> []
                | head::next ->
                    List.append (get_transitions_from_state2 head) (loop next) in
            loop a
        end
    | _ -> []

let print_transition_list t_list = List.iter (fun x -> print_transition x) t_list

let get_transition state head_value t_list =
    try
      t_list |> List.find (fun x -> 
            if (x.current_state = state) && (x.head_value = head_value) 
            then true else false)
    with
      _ -> Printf.printf "Transition with STATE : [%s] and HEAD : [%c] not found\n"
               state head_value; exit 0
    
let test1 () =
    let json = Yojson.Basic.from_file "test1.json" in
    let trans_obj = create_transition "test" json in
    print_transition trans_obj

let test2 () =
    let json = Yojson.Basic.from_file "test2.json" in
    let transitions = get_transitions_from_state json in
    List.iter (fun x -> print_transition x) transitions

let test3 () =
    let json = Yojson.Basic.from_file "test3.json" in
    let transitions = load_transitions_to_list json in
    get_transition "scanright" '.' transitions |> print_transition;
    get_transition "eraseone" '1' transitions |> print_transition;
    get_transition "subone" '1' transitions |> print_transition

(*let () = test3 ()*)


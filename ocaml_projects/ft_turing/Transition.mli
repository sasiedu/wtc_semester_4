(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   Transition.mli                                     :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2017/10/17 13:47:33 by sasiedu           #+#    #+#             *)
(*   Updated: 2017/10/19 16:34:45 by sasiedu          ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

type actions = LEFT | RIGHT

type transition = {
    current_state : string;
    head_value : char;
    next_state : string;
    write_value : char;
    action : actions;
}

val create_transition : string -> Yojson.Basic.json -> transition

val load_transitions_to_list : Yojson.Basic.json -> transition list

val print_transition : transition -> unit

val print_transition_list : transition list -> unit

val get_transition : string -> char -> transition list -> transition

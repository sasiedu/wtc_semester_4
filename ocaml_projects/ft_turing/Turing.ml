(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   Turing.ml                                          :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2017/10/19 14:18:51 by sasiedu           #+#    #+#             *)
(*   Updated: 2017/10/19 17:09:19 by sasiedu          ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let rec run_machine json_machine tape transition =
    tape |> Tape.CharTape.print;
    transition |> Transition.print_transition;
    (*List.iter 
      (fun final -> let tmp = String.sub final 1 ((String.length final)-2) in
         if (tmp = transition.Transition.next_state) then 
           exit 0) 
      json_machine.Jmachine.final_states;*)
    Tape.CharTape.write transition.Transition.write_value tape;
    let new_tape = match transition.Transition.action with
        | Transition.RIGHT ->
            Tape.CharTape.move_right tape json_machine.Jmachine.blank
        | Transition.LEFT ->
            Tape.CharTape.move_left tape json_machine.Jmachine.blank in
    List.iter 
      (fun final -> let tmp = String.sub final 1 ((String.length final)-2) in
         if (tmp = transition.Transition.next_state) then 
           begin new_tape |> Tape.CharTape.print; print_char '\n'; exit 0 end) 
      json_machine.Jmachine.final_states;
    run_machine json_machine new_tape 
        (Jmachine.get_transition json_machine 
           transition.Transition.next_state 
           (new_tape |> Tape.CharTape.read))

let create_and_load_tape json_machine args =
    let blank = json_machine |> Jmachine.get_blank in
    let input_len = args.Util.input |> String.length in
    let tape_machine = (0, Tape.CharTape.create (input_len * 2) blank) in
    let rec loop tape index = match index with
        | y when y >= input_len -> (0, tape |> snd)
        | _ -> Tape.CharTape.write args.Util.input.[index] tape;
                loop (Tape.CharTape.move_right tape blank)
                  (index + 1) in
    loop tape_machine 0

let check_universal_machine (args : Util.args_t) = match args.Util.json with
    | "universal.json" -> Universal.create_universal_machine args.Util.input 
    | _ -> Jmachine.create_json_machine args.Util.json

let new_args (args : Util.args_t) =
    let input = match args.Util.json with
        | "universal.json" -> 
                (fun str -> 
                    let splits = Str.split (Str.regexp "#") str in
                    List.nth splits 7
                ) args.Util.input
        | _ -> args.Util.input in
    {Util.help=args.Util.help; Util.json=args.Util.json; Util.input=input}

let build_machine (args : Util.args_t) =
    let json_machine = check_universal_machine args in
    let args_new = new_args args in
    String.iter (fun x -> 
        match (List.exists (fun y -> if y = x then true else false) 
                 (json_machine |> Jmachine.get_inputs)) with
        | true -> ()
        | false -> Printf.printf "Invalid input character : %c\n" x; exit 0
      ) args_new.Util.input;
    let tape_machine = create_and_load_tape json_machine args_new in
    json_machine |>Jmachine.print_json_machine;
    run_machine json_machine tape_machine 
        (Jmachine.get_transition json_machine
           json_machine.Jmachine.initial_state 
           (tape_machine |> Tape.CharTape.read))

let main () =
    let argv = Sys.argv in
    match (argv |> Array.length) with
    | 1 ->  Util.print_help ()
    | len -> let new_argv = Array.sub argv 1 (len - 1) in
             let args = Util.load_args 
                        (new_argv |> Array.to_list) 
                        (Util.create_args false "" "") in
             match args.Util.help with
             | true -> Util.print_help ()
             | false -> if args.Util.json = "" then
                            print_endline "Error : No json file"
                        else if args.Util.input = "" then
                            print_endline "Error : No input"
                        else
                            build_machine args

let () = main ()


let explode s =
    let rec exp i l =
        if i < 0 then l else exp (i - 1) (s.[i] :: l) in
    exp (String.length s - 1) []

let build_transition n_str = 
    {
        Transition.current_state = List.nth n_str 0;
        Transition.head_value = (List.nth n_str 1).[0];
        Transition.next_state = List.nth n_str 2;
        Transition.write_value = (List.nth n_str 3).[0];
        Transition.action = match (List.nth n_str 4) with
            | "L" -> Transition.LEFT
            | "R" -> Transition.RIGHT
            | _ -> print_endline 
                    ("Error : transition action -> " ^ (List.nth n_str 4));
                    exit 0
    }


let rec get_transitions n_list = match n_list with
    | [] -> []
    | head::next -> 
            let n_str = Str.split (Str.regexp "*") head in
            match n_str |> List.length with
            | y when y = 5 -> (build_transition n_str)::(get_transitions next)
            | _ ->  print_endline ("Error : transition line : " ^ head);
                    print_string "Format -> current_state*read*next_state*";
                    print_endline "write*action"; 
                    exit 0

let create_json_machine params = {
        Jmachine.name="Universal"; 
        Jmachine.work= (List.nth params 0) |> explode;
        Jmachine.inputs= (List.nth params 1) |> explode;
        Jmachine.blank= (List.nth params 2).[0];
        Jmachine.states= List.map (fun str -> "\"" ^ str ^ "\"")
            (Str.split (Str.regexp ",") (List.nth params 3));
        Jmachine.initial_state= List.nth params 4;
        Jmachine.final_states= List.map (fun str -> "\"" ^ str ^ "\"") 
            (Str.split (Str.regexp ",") (List.nth params 5));
        Jmachine.transitions= 
            get_transitions (Str.split (Str.regexp ",") (List.nth params 6))
    }

let create_universal_machine input_line =
    let splits = Str.split (Str.regexp "#") input_line in
    (fun () -> match splits |> List.length with
        | y when y = 8 -> ()
        | _ -> print_endline "Error : expected 8 groups";
               print_string "work_alphabets#input_alphabets#blank_alphabet#";
               print_string "states#initial_state#final_states#transitions#";
               print_endline "input";
               exit 0;
    )();
    let j_machine = create_json_machine splits in
    j_machine

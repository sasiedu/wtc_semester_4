(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   Util.ml                                            :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2017/10/19 14:47:47 by sasiedu           #+#    #+#             *)
(*   Updated: 2017/10/19 15:56:47 by sasiedu          ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

type args_t = {
    help : bool;
    json : string;
    input : string;
}

let create_args help json input = {help = help; json = json; input = input}

let rec load_args arg_list args = match arg_list with
    | [] -> args
    | head::next -> match head with
        | "--help" -> load_args next (create_args true args.json args.input)
        | "-h" -> load_args next (create_args true args.json args.input)
        | str -> let regexp = Str.regexp ".+\.json" in
                 match (Str.string_match regexp str 0) with
                 | true -> load_args next (create_args args.help str args.input)
                 | false -> load_args next (create_args args.help args.json str)

let print_help () =
  print_endline "usage: ft_turing [-h] jsonfile input\n";
  print_endline "positional arguments:";
  print_endline "\tjsonfile\t\tjson description of the machine";
  print_endline "\tinput\t\t\tinput of the machine\n";
  print_endline "optional arguments:";
  print_endline "\t-h, --help\t\tshow this help message and exit"

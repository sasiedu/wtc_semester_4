(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   Util.mli                                           :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2017/10/19 14:47:38 by sasiedu           #+#    #+#             *)
(*   Updated: 2017/10/19 15:45:43 by sasiedu          ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

type args_t = {
    help : bool;
    json : string;
    input : string;
}

val create_args : bool -> string -> string -> args_t

val load_args : string list -> args_t -> args_t

val print_help : unit -> unit
